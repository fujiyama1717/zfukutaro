package com.fukutarou.zfukutarou.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController

public class UserController {
    // @RequestMapping("/login")
    // public String login() {
    //     return "login";
    // }

    // @RequestMapping(value = "/")
    // private String hello() {
    //     return "menu";
    // }

    @RequestMapping("/user")
    public ModelAndView test(ModelAndView mav){
       mav.setViewName("user");
        return mav;
    }
}