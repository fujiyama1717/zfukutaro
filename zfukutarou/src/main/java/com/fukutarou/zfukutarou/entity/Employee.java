package com.fukutarou.zfukutarou.entity;

public class Employee {

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="weather")
public class Weather {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id_employee;
    
    private String nm_employee;
    
    private String kn_employee;
    
    private Integer mail_address;
    
    private Integer password;

    public Integer getId_employee() {
        return id_employee;
    }

    public void setId_employee(Integer id_employee) {
        this.id_employee = id_employee;
    }

    public String getNm_employee() {
        return nm_employee;
    }

    public void setNm_employee(String nm_employee) {
        this.nm_employee = nm_employee;
    }

    public String getKn_employee() {
        return kn_employee;
    }

    public void setKn_employee(String kn_employee) {
        this.kn_employee = kn_employee;
    }

    public Integer getMail_address() {
        return mail_address;
    }

    public void setMail_address(Integer mail_address) {
        this.mail_address = mail_address;
    }

    public Integer getPassword() {
        return password;
    }

    public void setPassword(Integer password) {
        this.password = password;
    }
    
}
}